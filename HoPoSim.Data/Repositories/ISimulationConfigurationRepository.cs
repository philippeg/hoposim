﻿using HoPoSim.Data.Domain;
using System.Collections.Generic;

namespace HoPoSim.Data.Repositories
{
    public interface ISimulationConfigurationRepository : IBaseRepository<SimulationConfiguration>
    {
        IEnumerable<SimulationConfiguration> GetAllIncludingAll();
    }
}
