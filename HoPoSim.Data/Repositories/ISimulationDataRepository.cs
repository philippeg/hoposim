﻿using HoPoSim.Data.Domain;
using System.Collections.Generic;

namespace HoPoSim.Data.Repositories
{
	public interface ISimulationDataRepository : IBaseRepository<SimulationData>
	{
		IEnumerable<SimulationData> GetAllIncludingAll();
	}
}
