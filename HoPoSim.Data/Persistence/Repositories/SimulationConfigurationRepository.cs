﻿using HoPoSim.Data.Domain;
using HoPoSim.Data.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace HoPoSim.Data.Persistence.Repositories
{
    public class SimulationConfigurationRepository : BaseRepository<SimulationConfiguration>, ISimulationConfigurationRepository
	{
		public SimulationConfigurationRepository(Context context)
			: base(context)
	{ }

	public IEnumerable<SimulationConfiguration> GetAllIncludingAll()
	{
		return base.Context.Set<SimulationConfiguration>()
			.Include(d => d.Results) //.ThenInclude(d => d.SimulationConfiguration)
			.Include(d => d.SimulationData).ThenInclude(d => d.Stapelqualität)
			.Include(d => d.SimulationData).ThenInclude(d => d.Baumart)
			.ToList();
	}

	public new Context Context
	{
		get { return Context as Context; }
	}
}
}
