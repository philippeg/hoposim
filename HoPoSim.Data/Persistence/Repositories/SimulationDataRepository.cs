﻿using HoPoSim.Data.Domain;
using HoPoSim.Data.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace HoPoSim.Data.Persistence.Repositories
{
	public class SimulationDataRepository : BaseRepository<SimulationData>, ISimulationDataRepository
	{
		public SimulationDataRepository(Context context)
			: base(context)
		{ }

		public IEnumerable<SimulationData> GetAllIncludingAll()
		{
			return base.Context.Set<SimulationData>()
				.Include(d => d.Baumart)
				.Include(d => d.Stapelqualität)
				.ToList();
		}

		public new Context Context
		{
			get { return Context as Context; }
		}
	}
}

