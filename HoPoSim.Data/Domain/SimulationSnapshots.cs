﻿namespace HoPoSim.Data.Domain
{
    public class SimulationSnapshot : BaseEntity
    {
        public string Data { get; set; }
    }
}
