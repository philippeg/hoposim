﻿using HoPoSim.Data.Domain;
using HoPoSim.Presentation.Extensions;

namespace HoPoSim.Presentation.ViewModels
{
	public class SimulationResultsViewModel 
	{
		public SimulationResultsViewModel(SimulationResults data = null)
		{
			_data = data;
		}
		private readonly SimulationResults _data;
		public string ToJsonResults()
		{
			var dao = _data.ToDAO();
			return IPC.DAO.Serializer<IPC.DAO.SimulationResults>.ToJSON(dao, false);
		}

		public string SimulationSnapshot
		{
			get { return _data.SimulationSnapshot.Data; }
		}

		public int IterationId
		{
			get { return _data.IterationId; }
		}

		public int Quality
		{
			get { return _data.Quality; }
		}

		public int FotooptikQuality
		{
			get { return _data.FotooptikQuality; }
		}

		public IterationResult IterationStatus
		{
			get { return _data.IterationStatus; }
		}

		public double StirnflächeV
		{
			get { return _data.StirnflächeV; }
		}

		public double StirnflächeH
		{
			get { return _data.StirnflächeH; }
		}

		public double FotooptikV
		{
			get { return _data.FotooptikV; }
		}

		public double FotooptikH
		{
			get { return _data.FotooptikH; }
		}

		public double Fotooptik
		{
			get { return _data.Fotooptik; }
		}

		public double FotooptikStützpunkteV
		{
			get { return _data.FotooptikStützpunkteV; }
		}

		public double FotooptikStützpunkteH
		{
			get { return _data.FotooptikStützpunkteH; }
		}

		public double PolygonzugV
		{
			get { return _data.PolygonzugV; }
		}

		public double PolygonzugH
		{
			get { return _data.PolygonzugH; }
		}

		public double Polygonzug
		{
			get { return _data.Polygonzug; }
		}

		public double SektionV
		{
			get { return _data.SektionV; }
		}

		public double SektionH
		{
			get { return _data.SektionH; }
		}

		public double Sektion
		{
			get { return _data.Sektion; }
		}

		public double PoltervolumeMR
		{
			get { return _data.PoltervolumeMR; }
		}

		public double PoltervolumeOR
		{
			get { return _data.PoltervolumeOR; }
		}

		public double PolterunterlagevolumeMR
		{
			get { return _data.PolterunterlagevolumeMR; }
		}

		public double PolterunterlagevolumeOR
		{
			get { return _data.PolterunterlagevolumeOR; }
		}

		public double Rindenanteil
		{
			get { return _data.Rindenanteil; }
		}

		public double UFSektionOR
		{
			get { return _data.UFSektionOR; }
		}

		public double UFSektionMR
		{
			get { return _data.UFSektionMR; }
		}

		public double UFPolygonzugOR
		{
			get { return _data.UFPolygonzugOR; }
		}

		public double UFPolygonzugMR
		{
			get { return _data.UFPolygonzugMR; }
		}

		public double UFFotooptikOR
		{
			get { return _data.UFFotooptikOR; }
		}

		public double UFFotooptikMR
		{
			get { return _data.UFFotooptikMR; }
		}

		public double Höhe
		{
			get { return _data.Höhe; }
		}

		public double Breite
		{
			get { return _data.Breite; }
		}
	}
}
